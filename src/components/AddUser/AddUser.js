import React, { Component } from "react";
import { timingSafeEqual } from "crypto";

class AddUser extends Component {
  constructor(props) {
    super(props);
    this.state = { name: '', age: '', balance: '', email: '', address: '' };

    this.handleAddUser = this.handleAddUser.bind(this);
  }
  handleAddUser() {
    console.log(this)
    const { name, age, balance, email, address } = this.state;

    fetch('/add-user', { method: 'POST', body: JSON.stringify({ name, age, balance, email, address })});
  }

  handleInputChange(e, name) {
    const value = e.target.value; 
    return this.setState(() => ({ [name]: value }));
  }

  render() {
    const { name, age, balance, email, address } = this.state;
    return (
      <div>
        <form>
          <label>Name: </label>
          <input name='name' value={name} onChange={(e) => this.handleInputChange(e, 'name')} />
          <label>Age: </label>
          <input name='age' value={age} onChange={(e) => this.handleInputChange(e, 'age')} />
          <label>Balance: </label>
          <input name='balance' value={balance} onChange={(e) => this.handleInputChange(e, 'balance')} />
          <label>Email: </label>
          <input name='email' value={email}  onChange={(e) => this.handleInputChange(e, 'email')} />
          <label>Address: </label>
          <input name='address' value={address}  onChange={(e) => this.handleInputChange(e, 'address')} />
          <button onClick={this.handleAddUser}>Add user</button>
        </form>
      </div>
    );
  }
}

export default AddUser;
