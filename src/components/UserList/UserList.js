import React from 'react';
import User from '../User/User';

const UserList = ({users}) => {
  return (
    <div>
      {users.map(u => <User key={`${u.name}-${u.email}`}{...u} />)}
    </div>
  );
};

export default UserList;