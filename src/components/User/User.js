import React from 'react';

const User = ({id, name, age, balance, email, address}) => {
  const handleUserDelete = id => fetch(`/users/${id}`, { method: 'DELETE' });

  return (
    <div>
      Name: {name} | Age: {age} | Balance: {balance} | E-mail: {email} | Address: {address} | <button onClick={() => handleUserDelete(id)}>Delete</button>
    </div>
  );
};

export default User;