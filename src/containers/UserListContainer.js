import React, { Component } from 'react';

import UserList from '../components/UserList/UserList';

class UserListContainer extends Component {
  constructor() {
    super();
    this.state = {
      users: [{id: 1, name: 'Zenek', age: 23, balance: 123, email: 'aaa@sss.com', address: 'Street'}],
    };
  }

  componentDidMount() {
    fetch('http://localhost:3000').then(({users}) => {
      this.setState(() => {
        return {
          users,
        };
      });
    });

  }

  render() {
    return (
      <div>
        <UserList users={this.state.users} />
      </div>
    );
  }
}

export default UserListContainer;