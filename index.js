import React from 'react';
import ReactDOM from 'react-dom';

import UserListContainer from './src/containers/UserListContainer';
import AddUser from './src/components/AddUser/AddUser';

const App = () => (
  <div className="App">
    <h1 className="App-Title">Hello Parcel x React</h1>
    <UserListContainer />
    <br/>
    <AddUser />
  </div>
);

ReactDOM.render(<App />, document.getElementById('root'));